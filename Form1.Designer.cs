﻿namespace testQuest
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.SaveBut = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.button4 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.CalculateBut = new System.Windows.Forms.Button();
            this.SumReqestLabel = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.SumReqestResLabel = new System.Windows.Forms.Label();
            this.ReqestResLabel = new System.Windows.Forms.Label();
            this.ReqestRkkResLabel = new System.Windows.Forms.Label();
            this.DataLabel = new System.Windows.Forms.Label();
            this.DateNowlabel = new System.Windows.Forms.Label();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.millsWork = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.Num = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Fio = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.requestRKK = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.request = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sumRequest = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Num,
            this.Fio,
            this.requestRKK,
            this.request,
            this.sumRequest});
            this.dataGridView1.Location = new System.Drawing.Point(12, 12);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dataGridView1.Size = new System.Drawing.Size(501, 223);
            this.dataGridView1.TabIndex = 0;
            // 
            // SaveBut
            // 
            this.SaveBut.Location = new System.Drawing.Point(583, 210);
            this.SaveBut.Name = "SaveBut";
            this.SaveBut.Size = new System.Drawing.Size(83, 25);
            this.SaveBut.TabIndex = 1;
            this.SaveBut.Text = "Сохранить\r\n\r\n";
            this.SaveBut.UseVisualStyleBackColor = true;
            this.SaveBut.Click += new System.EventHandler(this.SaveBut_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(499, 300);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(227, 20);
            this.button3.TabIndex = 2;
            this.button3.Text = "Открыть файл с обращениями";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.openRequst_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(499, 258);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(227, 20);
            this.button4.TabIndex = 3;
            this.button4.Text = "Открыть файл с РКК";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.openRequestRkk_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(13, 258);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(462, 20);
            this.textBox1.TabIndex = 4;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(13, 300);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(462, 20);
            this.textBox2.TabIndex = 5;
            // 
            // CalculateBut
            // 
            this.CalculateBut.Location = new System.Drawing.Point(583, 177);
            this.CalculateBut.Name = "CalculateBut";
            this.CalculateBut.Size = new System.Drawing.Size(83, 27);
            this.CalculateBut.TabIndex = 6;
            this.CalculateBut.Text = "Расчитать";
            this.CalculateBut.UseVisualStyleBackColor = true;
            this.CalculateBut.Click += new System.EventHandler(this.CalculateBut_Click);
            // 
            // SumReqestLabel
            // 
            this.SumReqestLabel.AutoSize = true;
            this.SumReqestLabel.Location = new System.Drawing.Point(535, 12);
            this.SumReqestLabel.Name = "SumReqestLabel";
            this.SumReqestLabel.Size = new System.Drawing.Size(37, 13);
            this.SumReqestLabel.TabIndex = 7;
            this.SumReqestLabel.Text = "Итого";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(535, 37);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(97, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Всего обращений";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(535, 63);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(121, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Всего обращений РКК";
            // 
            // SumReqestResLabel
            // 
            this.SumReqestResLabel.AutoSize = true;
            this.SumReqestResLabel.Location = new System.Drawing.Point(663, 11);
            this.SumReqestResLabel.Name = "SumReqestResLabel";
            this.SumReqestResLabel.Size = new System.Drawing.Size(0, 13);
            this.SumReqestResLabel.TabIndex = 10;
            // 
            // ReqestResLabel
            // 
            this.ReqestResLabel.AutoSize = true;
            this.ReqestResLabel.Location = new System.Drawing.Point(663, 37);
            this.ReqestResLabel.Name = "ReqestResLabel";
            this.ReqestResLabel.Size = new System.Drawing.Size(0, 13);
            this.ReqestResLabel.TabIndex = 11;
            // 
            // ReqestRkkResLabel
            // 
            this.ReqestRkkResLabel.AutoSize = true;
            this.ReqestRkkResLabel.Location = new System.Drawing.Point(663, 63);
            this.ReqestRkkResLabel.Name = "ReqestRkkResLabel";
            this.ReqestRkkResLabel.Size = new System.Drawing.Size(0, 13);
            this.ReqestRkkResLabel.TabIndex = 12;
            // 
            // DataLabel
            // 
            this.DataLabel.AutoSize = true;
            this.DataLabel.Location = new System.Drawing.Point(535, 89);
            this.DataLabel.Name = "DataLabel";
            this.DataLabel.Size = new System.Drawing.Size(33, 13);
            this.DataLabel.TabIndex = 13;
            this.DataLabel.Text = "Дата";
            // 
            // DateNowlabel
            // 
            this.DateNowlabel.AutoSize = true;
            this.DateNowlabel.Location = new System.Drawing.Point(663, 89);
            this.DateNowlabel.Name = "DateNowlabel";
            this.DateNowlabel.Size = new System.Drawing.Size(0, 13);
            this.DateNowlabel.TabIndex = 14;
            // 
            // millsWork
            // 
            this.millsWork.AutoSize = true;
            this.millsWork.Location = new System.Drawing.Point(646, 119);
            this.millsWork.Name = "millsWork";
            this.millsWork.Size = new System.Drawing.Size(0, 13);
            this.millsWork.TabIndex = 16;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(535, 119);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(105, 13);
            this.label4.TabIndex = 15;
            this.label4.Text = "Время выполнения";
            // 
            // Num
            // 
            this.Num.Frozen = true;
            this.Num.HeaderText = "№ п/п";
            this.Num.Name = "Num";
            this.Num.ReadOnly = true;
            this.Num.Width = 40;
            // 
            // Fio
            // 
            this.Fio.Frozen = true;
            this.Fio.HeaderText = "Ответственный исполнитель";
            this.Fio.Name = "Fio";
            this.Fio.ReadOnly = true;
            // 
            // requestRKK
            // 
            this.requestRKK.Frozen = true;
            this.requestRKK.HeaderText = "Количество неисполненных входящих документов";
            this.requestRKK.Name = "requestRKK";
            this.requestRKK.ReadOnly = true;
            // 
            // request
            // 
            this.request.Frozen = true;
            this.request.HeaderText = "Количество неисполненных письменных обращений граждан";
            this.request.Name = "request";
            this.request.ReadOnly = true;
            // 
            // sumRequest
            // 
            this.sumRequest.Frozen = true;
            this.sumRequest.HeaderText = "Общее количество документов и обращений";
            this.sumRequest.Name = "sumRequest";
            this.sumRequest.ReadOnly = true;
            // 
            // Form1
            // 
            this.ClientSize = new System.Drawing.Size(738, 339);
            this.Controls.Add(this.millsWork);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.DateNowlabel);
            this.Controls.Add(this.DataLabel);
            this.Controls.Add(this.ReqestRkkResLabel);
            this.Controls.Add(this.ReqestResLabel);
            this.Controls.Add(this.SumReqestResLabel);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.SumReqestLabel);
            this.Controls.Add(this.CalculateBut);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.SaveBut);
            this.Controls.Add(this.dataGridView1);
            this.Name = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button SaveBut;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Button CalculateBut;
        private System.Windows.Forms.Label SumReqestLabel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label SumReqestResLabel;
        private System.Windows.Forms.Label ReqestResLabel;
        private System.Windows.Forms.Label ReqestRkkResLabel;
        private System.Windows.Forms.Label DataLabel;
        private System.Windows.Forms.Label DateNowlabel;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.Label millsWork;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Num;
        private System.Windows.Forms.DataGridViewTextBoxColumn Fio;
        private System.Windows.Forms.DataGridViewTextBoxColumn requestRKK;
        private System.Windows.Forms.DataGridViewTextBoxColumn request;
        private System.Windows.Forms.DataGridViewTextBoxColumn sumRequest;
    }
}

