﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace testQuest
{
    public partial class Form1 : Form
    {
        public static string[] temp;
        Stopwatch stopWatch = new Stopwatch();
        bool calMs = false;


        public Form1()
        {
            InitializeComponent();
            dataGridView1.SortCompare += new DataGridViewSortCompareEventHandler(
            this.dataGridView1_SortCompare);
            dataGridView1.Sorted += new EventHandler(this.dataGridView1_num);

        }

        private void dataGridView1_num(object sender, EventArgs e)
        {
            foreach (DataGridViewRow dataGridView in dataGridView1.Rows)
            {
                dataGridView.Cells["Num"].Value = dataGridView.Index + 1;
            }
        }

        private void dataGridView1_SortCompare(object sender, DataGridViewSortCompareEventArgs e)
        {
            if (e.SortResult == 0 && e.Column.Name == "Fio")
            {
                e.SortResult = System.String.Compare(
                e.CellValue1.ToString(), e.CellValue2.ToString());
            }
            else
            {


                e.SortResult = int.Parse(e.CellValue1.ToString()).CompareTo(int.Parse(e.CellValue2.ToString()));


                if (e.SortResult == 0 && e.Column.Name == "requestRKK")
                    e.SortResult = int.Parse(dataGridView1.Rows[e.RowIndex1].Cells["request"].Value.ToString())
                        .CompareTo(int.Parse(dataGridView1.Rows[e.RowIndex2].Cells["request"].Value.ToString()));
                else if(e.SortResult == 0)
                    e.SortResult = int.Parse(dataGridView1.Rows[e.RowIndex1].Cells["requestRKK"].Value.ToString())
                        .CompareTo(int.Parse(dataGridView1.Rows[e.RowIndex2].Cells["requestRKK"].Value.ToString()));
                if (e.SortResult == 0)
                    e.SortResult = -System.String.Compare(
                dataGridView1.Rows[e.RowIndex1].Cells["Fio"].Value.ToString(),
                dataGridView1.Rows[e.RowIndex2].Cells["Fio"].Value.ToString());
            }

            e.Handled = true;
        }

        private void openRequestRkk_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
            textBox1.Text = openFileDialog1.FileName;
        }

        private void openRequst_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
            textBox2.Text = openFileDialog1.FileName;
        }

        private void CalculateBut_Click(object sender, EventArgs e)
        {
            Stopwatch stopWatch = new Stopwatch();
            DateNowlabel.Text = String.Format("{0:d}", DateTime.Now);
            stopWatch.Start();
            dataGridView1.Rows.Clear();
            SaveDava(textBox1.Text);
            AddData(temp, dataGridView1, "requestRKK");

            SaveDava(textBox2.Text);
            AddData(temp, dataGridView1, "request");

            calcSum(dataGridView1);
            stopWatch.Stop();
            foreach (DataGridViewRow dataGridView in dataGridView1.Rows)
            {
                dataGridView.Cells["Num"].Value = dataGridView.Index+1;
            }
            if (!calMs)
            {
                TimeSpan ts = stopWatch.Elapsed;
                millsWork.Text = $@"{ts.Milliseconds}ms";
                calMs = true;
            }

        }

        private static void SaveDava(string path)
        {
            if (File.Exists(path))
                temp = File.ReadAllLines(path);
            else
            MessageBox.Show("выбран некорректный файл: " + path, "Ошибка",
                            MessageBoxButtons.OK, MessageBoxIcon.Error);

        }

        public static object AddData(string[] data, DataGridView dataGridView, string request)
        {
            string IgnoreName = "Климов С.А.";
            int count = 0;
            foreach (var VARIABLE in data)
            {
                var split = Regex.Split(VARIABLE, $@"\t",
                    RegexOptions.IgnoreCase,
                    TimeSpan.FromMilliseconds(500));
                var split2 = Regex.Split(split[1], ";",
                    RegexOptions.IgnoreCase,
                    TimeSpan.FromMilliseconds(500));

                // создание Имени
                var name = ExctraxtIni(split[0]);
                int i = 0;
                while (name == IgnoreName)
                {
                    name = Regex.Replace(split2[i], @" \(Отв\.\)", "");
                    i++;
                }

                bool found = false;
                //запись/добавление данных
                //if (dataGridView.RowCount > 1)
                    foreach (DataGridViewRow currRow in dataGridView.Rows)
                    {
                    if (currRow.Cells["Fio"].Value != null)
                    if (currRow.Cells["Fio"].Value.Equals(name))
                        {
                            currRow.Cells[request].Value = Convert.ToInt32(currRow.Cells[request].Value) + 1;
                            found = true;
                            break;
                        }
                         
                }
                if (!found)
                {
                    var index = dataGridView.Rows.Add();
                    dataGridView.Rows[index].Cells["Fio"].Value = name;
                    dataGridView.Rows[index].Cells[request].Value = 1;
                    found = true;
                }

            }
                return count;
        }
        static string ExctraxtIni(string name)
        {
            var inits = Regex.Match(name, @"(\w+)\s+(\w+)\s+(\w+)").Groups;
            return string.Format("{0} {1}.{2}.", inits[1], inits[2].Value[0], inits[3].Value[0]);
        }

        void calcSum(DataGridView dataGridView)
        {
            var countRequest = 0;
            var countRequestRKK = 0;
            foreach (DataGridViewRow currRow in dataGridView.Rows)
            {
                /*if (currRow.Cells["request"].Value == null)
                    currRow.Cells["request"].Value = 0;
                else if(Convert.ToInt32(currRow.Cells["request"].Value) > 0)
                    countRequest++;*/

                if (Convert.ToInt32(currRow.Cells["request"].Value) > 0)
                    countRequest += (int)currRow.Cells["request"].Value;
                else
                    currRow.Cells["request"].Value = 0;

                /*if (currRow.Cells["requestRKK"].Value == null)
                    currRow.Cells["requestRKK"].Value = 0;
                else*/ if (Convert.ToInt32(currRow.Cells["requestRKK"].Value) > 0)
                    countRequestRKK += (int)currRow.Cells["requestRKK"].Value;
                else currRow.Cells["requestRKK"].Value = 0;

                currRow.Cells["sumRequest"].Value = Convert.ToInt32(currRow.Cells["request"].Value) + Convert.ToInt32(currRow.Cells["requestRKK"].Value);

            }

            
            getStatistic(ReqestResLabel, countRequest);
            getStatistic(ReqestRkkResLabel, countRequestRKK);
            getStatistic(SumReqestResLabel, countRequest + countRequestRKK);

        }

        void getStatistic(Label request, int count) => request.Text = count.ToString();

        private void SaveBut_Click(object sender, EventArgs e)
        {
            saveFileDialog1.Filter = "Текстовый документ (*.txt)|*.txt|Все файлы (*.*)|*.*";

            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                StreamWriter streamWriter = new StreamWriter(saveFileDialog1.FileName);
                streamWriter.WriteLine(new string(' ', 20) + $"{"Справка о неисполненных документах и обращениях граждан"}\n");
                streamWriter.WriteLine($"Не исполнено в срок {SumReqestResLabel.Text} документов, из них:");
                streamWriter.WriteLine($"- количество неисполненных входящих документов: {ReqestResLabel.Text};");
                streamWriter.WriteLine($"- количество неисполненных письменных обращений граждан: {ReqestRkkResLabel.Text}.");
                if (dataGridView1.SortedColumn != null)
                    streamWriter.WriteLine($"Сортировка по столбцу: {dataGridView1.SortedColumn.HeaderText}\n\n");
                else streamWriter.WriteLine($"Сортировки нет\n\n");
                streamWriter.WriteLine(string.Format("{0,19}|{1,19}|{2,19}|{3,19}|{4,19}|", "№", "Ответственный", "Количество ", "Количество", "Общее количество"));
                streamWriter.WriteLine(string.Format("{0,19}|{1,19}|{2,19}|{3,19}|{4,19}|", "п/п","исполнитель", "неисполненных ", "неисполненных ", "документов и"));
                streamWriter.WriteLine(string.Format("{0,19}|{0,19}|{1,19}|{2,19}|{3,19}|",  "", "входящих документов", "письменных", "обращений"));
                streamWriter.WriteLine(string.Format("{0,19}|{0,19}|{0,19}|{1,19}|{0,19}|", "", "обращений граждан"));
                foreach (DataGridViewRow row in dataGridView1.Rows)
                {
                    streamWriter.WriteLine(string.Format("{0, 19:D}|{1, 19}|{2 , 19:D}|{3, 19:D}|{4,19:D}|",
                        row.Cells["Num"].Value,
                        row.Cells["Fio"].Value, 
                        row.Cells["requestRKK"].Value,
                        row.Cells["request"].Value,
                        row.Cells["sumRequest"].Value));
                    streamWriter.WriteLine(new string('-', 100));
                }
                streamWriter.WriteLine($"\n\n\nДата составления справки: {DateNowlabel.Text}");
                streamWriter.Close();
            }

        }
    }
}
